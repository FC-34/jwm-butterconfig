#!/usr/bin/env sh

unset ans file term editor browser

cd "${0%/*}" || exit 1

# Print the LICENSE because it is important.
cat << EOF
 JWM Butter Config Installation Setup
 MIT License

 Copyright (c) 2019-2020 Lucas Cruz

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

EOF

printf " [*] Proceed with the installation? [y/N] " && read -r ans
if [ "$ans" = Y ] || [ "$ans" = y ]; then
  printf '\n'
else
  printf '%s\n' " [!] Installation canceled." && exit 0
fi

! command -v gxmessage >/dev/null 2>&1 &&
! command -v xmessage >/dev/null 2>&1 &&
printf " [!] WARN: xmessage OR gxmessage WAS NOT FOUND\n [!] Installation aborted." && exit 0

if [ -d $HOME/.config/jwm ]; then
  printf " [*] The folder \"$HOME/.config/jwm\" already exists, replace all contents? [y/N] " && read -r ans
  if [ "$ans" = Y ] || [ "$ans" = y ]; then
    rm -rf "$HOME/.config/jwm"
  else
    printf '%s\n' " [!] Installation aborted." && exit 0
  fi
fi

mkdir -p "$HOME/.config/jwm" &&
cp -r .config/ .jwmrc $HOME &&
cp .config/jwm/styles/default/main $HOME/.config/jwm/styles/current-style &&
cp .config/jwm/icons/default/main $HOME/.config/jwm/icons/current-icon

printf " [+] Define the default terminal emulator [xterm] " && read -r term
printf " [+] Define the default file manager [pcmanfm] " && read -r file
printf " [+] Define the default web browser [firefox] " && read -r browser
printf " [+] Define the default graphical text editor [leafpad] " && read -r editor

# Set the default ones if the user didn't.
term=${term:-xterm}
file=${file:-pcmanfm}
browser=${browser:-firefox}
editor=${editor:-leafpad}

# Check if those programs are installed and warn the user.
for c in "$term" "$file" "$browser" "$editor" "xprop"; do
  ! command -v "$c" >/dev/null 2>&1 && printf " [!] WARN: %s NOT FOUND\n" "$c"
done

cd $HOME || exit 1

for f in .config/jwm/rootmenu .config/jwm/bindings; do
  sed -i "s/<-TERM->/$term/g;
  s/<-FILE->/$file/g;
  s/<-BROWSER->/$browser/g;
  s/<-EDITOR->/$editor/g" "$f"
done

printf " [+] Instalation completed!\n"
