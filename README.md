# JWM Butter Config
The default configuration file of Joe's Window Manager can be modified quite a lot and this config separates each part of it
into its own file, making more clean and organized.

## Changes

There are a few changes and additions to make your life easier, if you plan to use JWM as your main window manager.

+ Use the left click menu for the applications and the right click menu for managing JWM.
+ Hardcoded launchers for most common applications. (Text editors, browsers, terminals and file managers.)
+ Dynamic Menus for Virtualbox and Steam. (Not enabled by default.)
+ Change Styles and Icons on the go.
+ Check for errors automaticaly and show it before restarting JWM, although it's not 100% reliable.

## Intended usage

When you make and clean installation of your system (e.g. Arch Linux) you might want to setup your things, and make everything be up and running
as quickly as you can, so having this config handy is useful.

You can install this config like this

```
git clone https://gitlab.com/soyteer/jwm-butterconfig
cd jwm-butterconfig
./install.sh
```

### Dependencies

+ `gxmessage` or `xmessage` and `xprop`.

## Optionals

### Recommended applications

These are the recommended applications for the start, after the installation. They're chosen because of it's lightweight and simplicity, but obviuosly
you can replace them later.

+ Terminal: XTerm
+ File Manager: PCManFM
+ Graphical Text Editor: Leafpad
+ Web Browser: Firefox
+ Wallpaper: Nitrogen or JWM's builtin.

Observation: In `menu/apps` there is a sample menu with alternatives for those ones, the menu is enabled by default.

## Merge Requests are welcome.
