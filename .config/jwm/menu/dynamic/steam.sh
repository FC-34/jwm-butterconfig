#
# STEAM by Derek Taylor (DistroTube)
# Ported to JWM by Lucas Cruz <lucascruzhl@gmail.com>
# A simple script that creates an JWM Dynamic menu that launches Steam games.
#
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License version 3 as published by the Free Software Foundation.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program. If not, see: http://www.gnu.org/licenses

STEAMAPPS=~/.local/share/Steam/steamapps
echo '<JWM>'
echo '<Program icon="steam" label="Steam">steam</Program>'
echo '<Separator/>'
for file in $(ls $STEAMAPPS/*.acf -1v); do
ID=$(grep '"appid"' $file | head -1 | sed -r 's/[^"]*"appid"[^"]*"([^"]*)"/\1/')
NAME=$(grep '"name"' $file | head -1 | sed -r 's/[^"]*"name"[^"]*"([^"]*)"/\1/')
echo "$NAME" | grep -Eq "Proton|Steam Linux Runtime|Steamworks Common Redistributables" && continue
echo "<Program label=\"$NAME\">steam steam://rungameid/$ID</Program>"
done
echo '</JWM>'
